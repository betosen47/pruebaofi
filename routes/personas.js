//*************************************************************** */
/* ********************** PERSONAS ***************************** */
/**************************************************************** */
var conexion = require('./../bin/conexionDB');
var express = require('express');
var router = express.Router();
//************************************************************** */
router.get('/', (req, res) =>{
    querySql = 'SELECT * FROM persona'; //Consulta en SQL
    conexion.query(querySql, (err, result) => {
      if (err){
        console.log('Error: '+err);
      } else{
        //renderizamos la VISTA listarProducto.ejs
        res.render('listarPersona', { datos: result }); 
      }
    });
  
  });
  
  router.get('/crear', (req, res)=>{
    //Renderizamos la Vista crearProducto.ejs
    res.render('crearPersona');
  });
  
  router.post('/guardar', (req, res) => {
    dni = req.body.dni;
    nombres = req.body.nombres;
    apellidos = req.body.apellidos;
    correo = req.body.correo;
    edad = req.body.edad;

    querySql = `INSERT INTO persona VALUES ('${dni}','${nombres}','${apellidos}','${correo}','${edad}');`
    conexion.query(querySql, (err, result)=>{
      if (err) {
        console.log('Error: ' + err);
      }
      else{
        
        res.redirect('http://localhost:3000/personas');
       
      }
    });
  });
  router.get('/eliminar/:dni', (req, res) => {
    let dni = req.params.dni;
  
    querySql = `DELETE FROM persona WHERE dni = '${dni}'`
    conexion.query(querySql, (err, result) => {
      if (err) {
        console.log('Error al eliminar')
      } else {
        console.log("Eliminado correctamente");
        //renderizamos la pagina listar Producto
        res.redirect('http://localhost:3000/personas');
      }
    })
  })
  router.get('/editar/:dni', (req, res) => {
    let dni = req.params.dni;
    querySql = `SELECT * FROM persona WHERE dni = '${dni}'`;
    conexion.query(querySql, (err, result) => {
      if (err) {
        console.log("Error: " + err);
      } else{
        //renderizamos la pagina editar producto enviandole datos
        res.render('editarPersona', { datos: result });
      }
    });
  });
  router.post('/editar/guardar', (req, res)=>{
    dni = req.body.dni;
    nombres = req.body.nombres;
    apellidos = req.body.apellidos;
    correo = req.body.correo;
    edad = req.body.edad;
    querySql = `UPDATE persona SET nombres='${nombres}', apellidos='${apellidos}' , correo='${correo}', edad='${edad}' WHERE dni='${dni}' `;
    conexion.query(querySql, (err, result) => {
      if (err){
        console.log('Error: '+ err);
      }
      else{
        console.log("Editado correctamente");
        //renderizamos la pagina listar Producto
        res.redirect('http://localhost:3000/personas');
      }
    });
  });
  module.exports = router;
  