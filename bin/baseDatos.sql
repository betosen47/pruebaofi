drop database DBdemo;

create database DBdemo;
use DBdemo;

create table Persona(
dni varchar(50) primary key,
nombres varchar(50),
apellidos varchar(50),
correo varchar(30),
edad varchar(2));

create table Producto (

codProducto varchar(6) primary key,
nombre varchar(30),
descripcion  varchar(100),
precio float(10,2));

create table Comprobante(
codComprobante varchar(6) primary Key,
dni varchar(8),
fecha varchar(10) ,
montoTotal float,
foreign key (dni) references Persona(dni));

create Table Detalle (
codDetalle varchar(6) primary key,
codComprobante varchar(6),
cantidad int,
codProducto varchar(6) ,
foreign key (codComprobante) references Comprobante(codComprobante),
foreign key (codProducto) references Producto(codProducto)
);