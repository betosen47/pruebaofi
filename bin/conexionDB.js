const mysql = require('mysql');

const pool = mysql.createPool({
    connectionLimit: 100,
    host: 'localhost',//127.0.0.1
    user: 'root',
    password: '',
    database: 'DBdemo'
});
module.exports = pool;